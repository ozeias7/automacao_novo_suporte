docker stop selenium-chrome
docker rm selenium-chrome

docker build -t suporte-multilaser-test .
docker run -d -p 4444:4444 --link suportemultilaseradmin_app_1 -e SCREEN_WIDTH=1920 -e SCREEN_HEIGHT=1080 --network=webproxy -v /dev/shm:/dev/shm --name selenium-chrome selenium/standalone-chrome
sleep 10
docker run --rm -i --link selenium-chrome --network=webproxy -v $(pwd)/tests:/usr/src/app suporte-multilaser-test

docker stop selenium-chrome
docker rm selenium-chrome