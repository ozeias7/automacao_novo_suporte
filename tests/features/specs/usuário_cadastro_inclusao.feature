#language:pt

@criarusuario_adicionar_outro
Funcionalidade: Realizar cadastro de usuário com adicionar outro


Cenario: Realizar cadastro de usuário com sucesso, clicando no botão Criar e Adicionar Outro
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678    |
E clicado no botao usuário
E clicado no no botão criar user
E informamos os dados abaixo:
|Nome  |E-mail                          |Password|
|teste10|teste10.silva@multilaser.com.br|12345678|
E clico no botão criar_adicionar_outro


Cenario: Realizar cadastro de usuário sem preencher o campo nome, clicando no botão Criar e Adicionar Outro
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao usuário
E clicado no no botão criar user
E informamos os dados abaixo:
|Nome  |E-mail                        |Password|
|      |teste1.silva@multilaser.com.br|12345678|
E clico no botão criar_adicionar_outro


Cenario: Realizar cadastro de usuário sem preencher o campo e-mail, clicando no botão Criar e adicionar outro
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao usuário
E clicado no no botão criar user
E informamos os dados abaixo:
|Nome  |E-mail|Password|
|teste2|      |12345678|
E clico no botão criar_adicionar_outro

Cenario: Realizar cadastro de usuário sem preencher o campo senha, clicando no botão Criar e adicionar outro
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao usuário
E clicado no no botão criar user
E informamos os dados abaixo:
|Nome  |E-mail                        |Password|
|teste3|teste3.silva@multilaser.com.br|        |
E clico no botão criar_adicionar_outro

Cenario: Realizar cadastro de usuário sem preencher o campo senha, clicando no botão Criar e adicionar outro
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao usuário
E clicado no no botão criar user
E informamos os dados abaixo:
|Nome  |E-mail |Password|
|      |       |        |
E clico no botão criar_adicionar_outro