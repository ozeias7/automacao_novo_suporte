#language:pt

@Cadastro_videos
Funcionalidade: Realizar cadastro de videos


Cenario: Realizar cadastro do video com sucesso.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao videos
E clicado no no botão criar videos
E  informamos o video abaixo:
|informarvideo                              |
|https://www.youtube.com/watch?v=QcTeFJGsWlg|
E clicar no campo ativo
E clico no botão criar video.


Cenario: Realizar cadastro do video não preenchedoo campo Ativo.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao videos
E clicado no no botão criar videos
E  informamos o video abaixo:
|informarvideo                              |
|https://www.youtube.com/watch?v=QcTeFJGsWlg|
E clico no botão criar video.


Cenario: Realizar cadastro do video não preenchedo o campo URL VIDEO.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao videos
E clicado no no botão criar videos
E clicar no campo ativo
E clico no botão criar video.

Cenario: Realizar cadastro do video não preenchedo o campo URL VIDEO e ATIVO
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao videos
E clicado no no botão criar videos
E clico no botão criar video.

Cenario: Realizar cadastro do video preenchendo o campo video, sem https.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicado no botao videos
E clicado no no botão criar videos
E  informamos o video abaixo:
|informarvideo                      |
|www.youtube.com/watch?v=QcTeFJGsWlg|
E clicar no campo ativo
E clico no botão criar video.



