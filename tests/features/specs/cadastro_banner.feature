#language:pt

@Cadastro_Banners
Funcionalidade: Realizar compra de produto


Cenario: Realizar cadastro do banner com sucesso com Duvidas Frequentes
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner          |
|Principal banner|
E selecionar o campo Tipo Duvidas Frequentes.
E clico na tela.
E selecioanr um arquivo.
E clicar  no botao criar banner

Cenario: Realizar cadastro do banner com sucesso com Pagina Inicial
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner          |
|Principal banner|
E selecionar o campo Tipo Pagina Inicial.
E clico na tela.
E selecioanr um arquivo.
E clicar  no botao criar banner

Cenario: Realizar cadastro do banner com sucesso com Drives e Manuais
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner          |
|Principal banner|
E selecionar o campo Tipo Drives e Manuais.
E clico na tela.
E selecioanr um arquivo.
E clicar  no botao criar banner

Cenario: Realizar cadastro do banner com sucesso com Posto de Atendimento
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner          |
|Principal banner|
E selecionar o campo Tipo Posto de Atendimento.
E clico na tela.
E selecioanr um arquivo.
E clicar  no botao criar banner


Cenario: Realizar cadastro do banner não preenchendo o campo Banner
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner|
|      |
E selecionar o campo Tipo Duvidas Frequentes.
E clico na tela.
E selecioanr um arquivo.
E clicar  no botao criar banner

Cenario: Realizar cadastro do banner e não selecionar o cmapo Tipo
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner          |
|Principal banner|
E clico na tela.
E selecioanr um arquivo.
E clicar  no botao criar banner

Cenario: Realizar cadastro do banner e não selecionar o arquivo
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner          |
|Principal banner|
E selecionar o campo Tipo Duvidas Frequentes.
E clico na tela.
E clicar  no botao criar banner


Cenario: Realizar cadastro do banner não preechedo os campos Banner, Tipo e arquivo
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de banner.
E clico no botão criar banner
E preencho os dados de cadastro:
|Banner|
|      |
E clico na tela.
E clicar  no botao criar banner