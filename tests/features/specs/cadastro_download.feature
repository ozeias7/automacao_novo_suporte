#language:pt

@Cadastro_download
Funcionalidade: Realizar cadastro do Download

Cenario: Realizar cadastro do Download com sucesso
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de Tipos de Download.
E clico no botão criar tipo de Download
E preencho os dados abaxo 
|Tipo Download|
|Principal    |
E selecioanr um arquivo Download.
E clicar no botao criar tipo Download

Cenario: Realizar cadastro do Download sem preencher o campo Tipo Download
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de Tipos de Download.
E clico no botão criar tipo de Download
E preencho os dados abaxo 
|Tipo Download|
|             |
E selecioanr um arquivo Download.
E clicar no botao criar tipo Download


Cenario: Realizar cadastro do Download sem selecionar o arquivo
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de Tipos de Download.
E clico no botão criar tipo de Download
E preencho os dados abaxo 
|Tipo Download|
|Principal    |
E clicar no botao criar tipo Download


Cenario: Realizar cadastro do Download sem preencher os campo Tipo Download e arquivo
Dado Dado que tenho acesso  ao sistea de ADM
E preenchemos dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico na opção de Tipos de Download.
E clico no botão criar tipo de Download
E preencho os dados abaxo 
|Tipo Download|
|             |
E clicar no botao criar tipo Download



