#language:pt

@login
Funcionalidade: Realizar login


Cenario: Realizar o login com sucesso.
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clico no botão logar

Cenario: Realizar o login, sem informar email.
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail |informarsenha|
|              |multi1234    |
E clico no botão logar

Cenario: Realizar o login, sem informar senha.
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail                  |informarsenha|
| ozeias.silva@multilaser.com.br|             |
E clico no botão logar

Cenario: Realizar o login, sem informar email e senha.
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail|informarsenha|
|             |             |
E clico no botão logar

Cenario: Realizar o login, com usuário invalido
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail               |informarsenha|
|ozeias.silva@multilaser.com.|12345678     |
E clico no botão logar

Cenario: Realizar o login, com senha invalida
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345        |
E clico no botão logar

Cenario: Realizar o login, com senha e usuário invalido
Dado Acesso  ao sistea de ADM
E preenchemos os dados da login abaixo:
|informaremail           |informarsenha|
|ozeias.silva@multilaser |12345        |
E clico no botão logar


