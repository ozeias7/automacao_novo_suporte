#language:pt

@cadastrar_contato_inclusão
Funcionalidade: Realizar cadastro de contato


Cenario: Realizar cadastro de contato com sucesso.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar contato
E clicar no botão criar contato
E preenche os campos abaixo:
|Nome  |E-mail          |
|teste4|teste2@gmail.com|
E clica no botão criar e adcionar


Cenario: Realizar cadastro de contato sem preencher o campo nome
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar contato
E clicar no botão criar contato
E preenche os campos abaixo:
|Nome  |E-mail          |
|      |teste2@gmail.com|
E clica no botão criar e adcionar

Cenario: Realizar cadastro de contato sem preencher o campo e-mail
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar contato
E clicar no botão criar contato
E preenche os campos abaixo:
|Nome  |E-mail|
|teste4|      |
E clica no botão criar e adcionar

Cenario: Realizar cadastro de contato sem preencher o campo e-mail e nome
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar contato
E clicar no botão criar contato
E preenche os campos abaixo:
|Nome |E-mail|
|     |      |
E clica no botão criar e adcionar