#language:pt

@cadastrar_perguntas_respostas
Funcionalidade: Realizar cadastro de contatos de respostas


Cenario: Realizar cadastro de contatos e respostas com sucesso.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao perguntas e respostas
E clicar no botão criar perguntas e respostas
E selecionar o site multi
E selecionar o tipo de site
E preenche os campo pergunta
|Pergunta|
|teste1  |
E preenche o campo resposta
|Resposta|
|teste1  |
E clico no botão criar pergunta resposta


Cenario: Realizar cadastro de contatos e respostas sem preencher o campo Pergunta
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao perguntas e respostas
E clicar no botão criar perguntas e respostas
E selecionar o site multi
E selecionar o tipo de site
E preenche os campo pergunta
|Pergunta|
|        |
E preenche o campo resposta
|Resposta|
|teste1  |
E clico no botão criar pergunta resposta

Cenario: Realizar cadastro de contatos e respostas sem preenhcer o campo Resposta
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao perguntas e respostas
E clicar no botão criar perguntas e respostas
E selecionar o site multi
E selecionar o tipo de site
E preenche os campo pergunta
|Pergunta|
|teste1  |
E preenche o campo resposta
|Resposta|
|        |
E clico no botão criar pergunta resposta

Cenario: Realizar cadastro de contatos e respostas sem preenhcer o campo Resposta e Pergunta
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao perguntas e respostas
E clicar no botão criar perguntas e respostas
E selecionar o site multi
E selecionar o tipo de site
E preenche os campo pergunta
|Pergunta|
|        |
E preenche o campo resposta
|Resposta|
|        |
E clico no botão criar pergunta resposta