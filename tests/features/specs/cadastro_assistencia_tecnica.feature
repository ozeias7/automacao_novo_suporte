#language:pt

@cadastrar_assitencia_tecnica
Funcionalidade: Realizar cadastro de assistencia tecnica


Cenario: Realizar cadastro de Assistencia Tecnica preenchendo o site Giga e Posto de Atendimento.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao Assistência Tecnica
E clicar no botão criar Assistência Tecnica
E clicar no site Giga
E clica no campo Assistencia Tecnica
E seleciona o tipo de Assistencia Tecnica Posto de Atendimento
E clico no botão criar Criar Assistencia Técnica


Cenario: Realizar cadastro de Assistencia Tecnica preenchendo o site Giga e Assistencia Técnica.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao Assistência Tecnica
E clicar no botão criar Assistência Tecnica
E clicar no site Giga
E clica no campo Assistencia Tecnica
E seleciona o tipo de Assistencia Tecnica
E clico no botão criar Criar Assistencia Técnica

Cenario: Realizar cadastro de Assistencia Tecnica não preenchendo o site Giga.
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao Assistência Tecnica
E clicar no botão criar Assistência Tecnica
E clica no campo Assistencia Tecnica
E seleciona o tipo de Assistencia Tecnica
E clico no botão criar Criar Assistencia Técnica

Cenario: Realizar cadastro de Assistencia Tecnica não preenchendo o site Giga e Assistencia Técnica
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao Assistência Tecnica
E clicar no botão criar Assistência Tecnica
E clica no campo Assistencia Tecnica
E clico no botão criar Criar Assistencia Técnica

Cenario: Realizar cadastro de Assistencia Tecnica preenchendo o site Giga e não Assistencia Técnica
Dado Dado que tenho acessado o ADM
E Realizado o login.
|informaremail                 |informarsenha|
|ozeias.silva@multilaser.com.br|12345678     |
E clicar no botao Assistência Tecnica
E clicar no botão criar Assistência Tecnica
E clicar no site Giga
E clica no campo Assistencia Tecnica
E clico no botão criar Criar Assistencia Técnica
