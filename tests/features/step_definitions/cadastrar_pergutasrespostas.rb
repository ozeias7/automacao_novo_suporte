Dado("clicar no botao perguntas e respostas") do
    menu.clicar_perguntas_Respostas
    sleep(2)
end
  
  Dado("clicar no botão criar perguntas e respostas") do
    menucadastrorespostas.btn_criarpergunta
    sleep(2)
end
  
  Dado("selecionar o site multi") do
    perguntasrespostas.btn_sitegiga
    sleep(2)
  end
  
  Dado("selecionar o tipo de site") do
    perguntasrespostas.btn_tiposite
    sleep(2)
end
  
  Dado("preenche os campo pergunta") do |table|
    perguntasrespostas.campo_pergunta(table.hashes[0])
    sleep(2)
  end

  
  Dado("preenche o campo resposta") do |table|
    perguntasrespostas.campo_resposta(table.hashes[0])
    sleep(2)
  end
  
  Dado("clico no botão criar pergunta resposta") do
    perguntasrespostas.botao_botaocriarpergunta
    sleep(2)
end