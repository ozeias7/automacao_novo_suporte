Dado("Dado que tenho acessado o ADM") do
    login.load
    sleep (1)
end
  
  Dado("Realizado o login.") do |table|
    login.loginusuario(table.hashes[0])
    sleep(2)

    login.logar
    sleep (2)
  end
  
  Dado("clicado no botao videos") do
    menu.clicar_videos
    sleep(1)
  end
  
  Dado("clicado no no botão criar videos") do
    menuvideo.botao_criar_video
    sleep(2)
  end
  
  Dado("informamos o video abaixo:") do |table|
    cadastrovideo.cria_video(table.hashes[0])
    sleep(2)
  end
  
  Dado("clicar no campo ativo") do
    cadastrovideo.clicar_ativo
  end
  
  Dado("clico no botão criar video.") do
    cadastrovideo.btn_criar_video
    sleep(2)
    
end