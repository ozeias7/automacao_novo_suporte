require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require_relative 'helper.rb'
require_relative 'page_helper.rb'


AMBIENTE = ENV['AMBIENTE']

CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")
World(Helper)
World(Pages)


# Configuraçao da URL
Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app,
        :browser => :remote,
        :desired_capabilities => :chrome,
        :url => "http://selenium-chrome:4444/wd/hub"
    )
end

Capybara.configure do |config|
    config.default_driver = CONFIG['browser'] == 'remote' ? :chrome : :selenium_chrome
    config.app_host = CONFIG['url_padrao']
    config.default_max_wait_time = 10
    config.server_port = 4444
end

Capybara.page.driver.browser.manage.window.maximize

at_exit do
    begin
        e = $! # last exception
        cleanup_after_tests
    ensure
        raise e if $! != e
    end
end