Dir[File.join(File.dirname(__FILE__), "../pages/*_pages.rb")].each {|file| require file}

module Pages
   
     def login
         @login ||= Login.new 
     end

     def banner
         @banner ||= Banner.new
     end

     def menubanner
         @menubanner ||= MenuBanner.new
     end

     def tipos_download
         @tipos_download ||= Tipos_download.new
     end

     def menu_download
         @menu_download ||= MenuDownload.new
     end
 
    
     def cadastardownload
         @cadastardownload ||= CadastroDownload.new
     end

     def menuarquivo
         @menuarquivo ||= MenuArquivo.new
     end

     def cadastroarquivo
         @cadastroarquivo ||= CadastroArquivo.new
     end


     def menu
         @menu ||= Menu.new
     end

     def menuusuario
         @menuusuario ||= MenuUsuario.new
     end

     def cadastrousuario
        @cadastrousuario ||= CadastroUsuario.new
    end

     def menuvideo
         @menuvideo ||= MenuVideo.new
     end

     def cadastrovideo
         @CadastroVideo ||= CadastroVideo.new
     end

   

     def produtos
         @produtos ||= Produtos.new
     end

     def perguntas_repostas
         @perguntas_respostas ||= Produtos_Respostas.new
     end

      def menucontato
          @menucontato ||= MenuContato.new
      end

     def contato
         @contato ||= Contato.new
     end


     def arquivos
         @arquivos ||= Arquivos.new
     end


     def menuAssistencia
        @menuAssistencia ||= MenuAssistencia.new
     end

     def cadastroAssistencia
         @cadastroAssistencia ||= CadastroAssistencia.new
     end


     def menucadastrorespostas
         @menucadastrorespostas ||= MenuCadastroRespostas.new
     end

     def perguntasrespostas
        @perguntasrespostas ||= PerguntasRespostas.new
     end


end
