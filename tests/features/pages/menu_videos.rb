class MenuVideo <SitePrism::Page

set_url '/nova/resources/videos'

# Atributos

element :ver, :xpath,  '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr/td[4]/span[1]/a/svg'
element :editar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr/td[4]/span[2]/a/svg'
element :excluir, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr/td[4]/button/svg'
element :criar_video, :xpath, '/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div[2]/a'


# Métodos

def botao_ver
    ver.click
end

    def botao_editar
        ver.click
    end
 
       def botao_excluir
           excluir.click
        end

            def botao_criar_video
                criar_video.click
            end





end