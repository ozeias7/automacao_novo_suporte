class MenuArquivo <SitePrism::Page

    #Elementos

    element :criararquivo, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div[2]/a'
    element :excluir, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[6]/button/svg'
    element :ver, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[6]/span[1]/a/svg'
    element :editar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[6]/span[2]/a/svg'


    # Métodos

    def btn_criararquivo
        criararquivo.click
    end


    

end
