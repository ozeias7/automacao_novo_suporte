class MenuUsuario <SitePrism::Page

    set_url '/nova/resources/users'
    
    # Atributos
    
    element :ver, :xpath,  '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[6]/span[1]/a/svg'
    element :editar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[6]/span[2]/a/svg' 
    element :excluir, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[6]/button/svg'
    element :criar_usuario, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div[2]'
    
    
    # Métodos
    
    def botao_ver
        ver.click
    end
    
        def botao_editar
            ver.click
        end
     
           def botao_excluir
               excluir.click
            end
    
                def botao_criar_usuário
                    criar_usuario.click
                end
    
    
    
    
    
    end