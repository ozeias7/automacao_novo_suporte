class  Menu <SitePrism::Page

set_url '/nova'

# Atributos

 element :banners, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[2]/a'
 element :tipos_download, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[3]/a'
 element :perguntas_Respostas, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[4]/a'
 element :arquivos, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[5]/a'
 element :contatos, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[7]/a'
 element :assitencia_tecnica, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[10]/a'
 element :usuario, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[11]/a'   
 element :videos, :xpath, '//*[@id="nova"]/div/div[1]/ul/li[12]/a'


# Métodos

   def clicarbanners
       banners.click
   end

        def clicartipos_download
            tipos_download.click
        end

        def clicar_perguntas_Respostas
            perguntas_Respostas.click
        end

        def clicar_arquivos
            arquivos.click
        end

        def clicar_contatos
            contatos.click
        end

        def clicar_produtos
            produtos.click
        end

        def clicar_assistencia_tecnica
            assitencia_tecnica.click
        end

        def clicar_usuario
            usuario.click
        end

        def clicar_videos
            videos.click
        end



end