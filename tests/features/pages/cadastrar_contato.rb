class Contato <SitePrism:: Page

# Atributos

element :nome, :xpath, '//*[@id="name"]'
element :email, :xpath, '//*[@id="email"]'

element :criarcontato, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[5]/button[2]/span'
element :criaeadicionar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[5]/button[1]/span'


# Métodos

def camponome(contato)
    nome.set contato['Nome']
    email.set contato['E-mail']
end

def btn_criarcontato
    criarcontato.click
end

def btn_criareadicionar
    criaeadicionar.click
end


end