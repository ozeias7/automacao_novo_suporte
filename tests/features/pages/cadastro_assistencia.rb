class CadastroAssistencia <SitePrism::Page

    set_url '/nova/resources/technical-assistances'
   
    # Elementos

     element :sitemultilaser, :xptah, '//*[@id="site"]/option[2]'
     element :sitegiga, :xptah, '//*[@id="site"]/option[3]'
     element :clicAssistencia, '[dusk="gol-technical-assistances-search-input"]'
     element :escreverAssistencia, :xptah, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[3]/div/div[2]/div[1]/div[2]' 
     element :tipoAssistenciaTecnica, :xptah, '//*[@id="type"]/option[2]'
     element :tipoAssistenciaPostoAtendimento, :xptah, '//*[@id="type"]/option[3]'
     
     element :botaocriarassitencia, '[dusk="create-button"]'

    # Métodos

   
      def opcao_siteGiga
        evaluate_script("setTimeout(function() {
          document.querySelector('#site').__vue__.$emit('input','gigasecurity');
        },2000);")
        sleep(10)
      end

      def opcao_siteMultulaser
        evaluate_script("setTimeout(function() {
          document.querySelector('#site').__vue__.$emit('input','multilaser');
        },2000);")
        sleep(10)
      end

      def click_clicAssistencia
        clicAssistencia.click
        evaluate_script("setTimeout(function() {
                document.querySelector('[dusk=gol-technical-assistances-search-input]').__vue__.$emit('selected', {
                    display: 'MULTI PARTS-ELETROPORTATEIS-DESKTOP-VEICULOS ELETRICOS-CAIXAS DE SOM-TELEVISORES',
                    value: 519
                });
          },2000);")
          sleep(2)  
      end

      
    
      def opcaotipo_tipoAssistenciaTecnica
        evaluate_script("setTimeout(function() {
          document.querySelector('#type').__vue__.$emit('input','assistencia-tecnica');
        },2000);")
        sleep(10)
      end


      def opcaotipo_tipoAssistenciaPostoAtendimento
        evaluate_script("setTimeout(function() {
          document.querySelector('#type').__vue__.$emit('input','posto-de-atendimento');
        },2000);")
        sleep(10)
      end
    

      def btn_botaocriarassitencia
        botaocriarassitencia.click
      end


end