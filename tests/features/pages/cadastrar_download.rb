class CadastroDownload <SitePrism::Page


    # Atributos

    element :campobanner, :xpath, '//*[@id="name"]'
    element :ativo, :xpath, '//*[@id="b-link"]'
    element :arquivo, '#file-icon'
    element :criartipodownload, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[5]/button[2]'


    # Métodos
   

    def campo_banner(cadastardownload)
        campobanner.set cadastardownload['Tipo Download'] 
    end

    def campoarquivo
        arquivo.click
    end

    def btn_criartipodownload
       criartipodownload.click
    end


end