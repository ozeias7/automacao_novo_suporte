class PerguntasRespostas <SitePrism:: Page



  # Atributos

  element :sitemutilaser, :xpath, '//*[@id="site"]/option[2]'
  element :sitegiga, :xpath, '//*[@id="site"]/option[3]'
  element :tiposite, :xpath, '//*[@id="type"]/option[2]'
  element :tipoblog, :xpath, '//*[@id="type"]/option[3]'
  element :tipomutiajuda, :xpath, '//*[@id="type"]/option[4]'

  element :pergunta, :xpath, '//*[@id="question"]'
  element :resposta, :xpath, '//*[@id="editor-js-answer"]/div/div[1]/div/div/div'

  element :botaocriarpergunta, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[9]/button[2]'
  element :botaocriaradiicionar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[7]/button[1]/span'



  # Métodos

  def btn_sitemutilaser
     sitemutilaser.click
  end

  def btn_sitegiga
      sitegiga.click
  end

  def btn_tiposite
      tiposite.click
  end

  def btn_tipoblog
      tipoblog.click
  end

  def btn_tipomutiajuda
      tipomutiajuda.click
  end


  def campo_pergunta(perguntasrespostas)
      pergunta.set perguntasrespostas['Pergunta']
  end

  def campo_resposta(perguntasrespostas)
      resposta.set perguntasrespostas['Resposta']
  end

 # Botoes 

  def botao_botaocriarpergunta
      botaocriarpergunta.click
  end

  def botao_botaocriaradiicionar
      botaocriaradiicionar.click
  end



end