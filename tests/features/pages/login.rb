class Login <SitePrism::Page

    set_url '/nova/login'

   # Elementos

   element :email, :xpath, '//*[@id="email"]'
   element :senha,'#password'
   element :login, :xpath, '/html/body/div/div/form/button'


   # Métodos
     def loginusuario(login)
       email.set login['informaremail'] 
       senha.set login['informarsenha']
     end

      def logar
          login.click
      end


end