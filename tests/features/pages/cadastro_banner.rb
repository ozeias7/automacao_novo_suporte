class Banner <SitePrism::Page

    set_url 'nova/resources/banners/new?viaResource=&viaResourceId=&viaRelationship='

  # Atributos

  element :campobanner, :xpath, '//*[@id="name"]'
  element :campoTipo, :xpath, '//*[@id="type"]/option[3]' 
  element :tela, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[5]/div/div[2]'
  element :campoAtivo, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[5]/span[1]/a/svg'
  element :campoarquivo, '#file-file'
  
  element :botaobanner, '[dusk="create-button"]'

 # Métodos

   def campo_banner(banner)
       campobanner.set banner['Banner'] 
   end

   def btn_tipo_duvidas
      evaluate_script("setTimeout(function() {
        document.querySelector('#type').__vue__.$emit('input','faq');
      },2000);")
      sleep(10)
    end


    def btn_tipo_pagina_inicial
        evaluate_script("setTimeout(function() {
          document.querySelector('#type').__vue__.$emit('input','home');
        },2000);")
        sleep(10)
      end

      def btn_tipo_drives_manuais
        evaluate_script("setTimeout(function() {
          document.querySelector('#type').__vue__.$emit('input','drivers-and-manuals');
        },2000);")
        sleep(10)
      end

      
      def btn_tipo_posto_atendimento
        evaluate_script("setTimeout(function() {
          document.querySelector('#type').__vue__.$emit('input','service-stations');
        },2000);")
        sleep(10)
      end


   def tipotela
       tela.click
   end

   def btn_ativo
       campoAtivo.click
   end

   def btn_arquivo
       campoarquivo.click
   end

   def btn_criar_banneer
       botaobanner.click
   end

   
end