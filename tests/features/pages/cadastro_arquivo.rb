class CadastroArquivo <SitePrism::Page

    # Elementos
    
    element :nomearquivo, :xpath, '//*[@id="name"]'
    element :tipodownload, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[2]/div/div[2]'
    element :arquivo, '#file-file'
    element :criararquivo, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[7]/button[2]'

    # Métodos


    def campo_nome_do_arquivo(cadastroarquivo)
        nomearquivo.set cadastroarquivo['NomedoArquivo']
        evaluate_script("setTimeout(function() {
            var selector = '[dusk=downloadType]'
            var value = 5
            var event = new Event('change')
            var el = document.querySelector(selector)
            el.value = value
            el.dispatchEvent(event)
        },2000);")
    end

    def btn_tela
        tipodownload.click
    end

    def btn_arquivo
        arquivo.click
    end

     def btn_criararquivo
       criararquivo.click
     end


end