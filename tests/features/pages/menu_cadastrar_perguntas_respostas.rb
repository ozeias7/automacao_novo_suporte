class MenuCadastroRespostas <SitePrism:: Page


    # Atributos

     element :criarpergunta, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div[2]/a'
     element :excluir, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[5]/button/svg'
     element :ver, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[5]/span[1]/a/svg'
     element :editar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[1]/td[5]/span[2]/a/svg'
     

    # Métodos

      def btn_criarpergunta
          criarpergunta.click
      end

      def btn_excluir
          excluir.click
      end

      def btn_ver
          ver.click
      end

      def btn_editar
        editar.click
      end




end