class CadastroVideo <SitePrism::Page


# Atributos

element :url_youtube, :xpath, '//*[@id="url"]'
element :ativo, :xpath, '/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/form/div[3]/div/div[2]/div[1]/div'
element :cancelar, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[4]/a'
element :criar_adicionar_video, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[4]/button[1]/span'
element :criar_video, :xpath, '//*[@id="nova"]/div/div[2]/div[2]/div/div[2]/div/form/div[4]/button[2]/span'



# Métodos
    
# Métodos
    def cria_video(cadastrovideo)
        url_youtube.set cadastrovideo['informarvideo'] 
    end

    def clicar_ativo
        ativo.click
    end

    def clicar_cancelar
        cancelar.click
    end

    def botao_adicionar_video
        criar_adicionar_video.click
    end

    def btn_criar_video
        criar_video.click
    end


end