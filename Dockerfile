FROM ruby:2.4.2
RUN apt-get update \
  && apt-get install libxi6 \
  libnss3 \
  libgconf-2-4 \
  fonts-liberation \
  libappindicator3-1 \
  libasound2 \
  libatk-bridge2.0-0 \
  libatk1.0-0 \
  libcups2 \
  libgtk-3-0 \
  libx11-xcb1 \
  libxss1 \
  lsb-release \
  xdg-utils \
  libxcomposite1 -y

RUN mkdir /usr/src/app
RUN mkdir /usr/src/app/output
WORKDIR /usr/src/app
ADD ./tests/ .

RUN bundle install
CMD ["cucumber", "-p", "producao", "--format", "html", "--out=report/relatorio.html"]